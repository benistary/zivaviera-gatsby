import React from 'react';
import { Container, Row, Col } from 'react-grid-system';
import { graphql } from 'gatsby';
import Image from 'gatsby-image';

import Layout from '../layouts/layout';
import { Seo, PageSection, Input } from '../components';

const KitchenPage = ({ data }) => (
  <Layout>
    <Seo title="Kitchen" keywords={[`gatsby`, `application`, `react`]} />
    <PageSection space="large">
      <Container>
        <Row>
          <Col md={6} offset={{ md: 3 }}>
            <Image fluid={data.astronaut.childImageSharp.fluid} />
          </Col>
        </Row>
      </Container>
    </PageSection>
    <PageSection space="large">
      <Container>
        <Row>
          <Col>
            <Input placeholder="12.09.2019" id="datum" label="Zadajte dátum" />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <Input placeholder="Meno" id="meno" label="Zadajte vaše meno" />
          </Col>
          <Col xs={6}>
            <Input
              placeholder="Priezvisko"
              id="priezvisko"
              label="Zadajte vaše priezvisko"
            />
          </Col>
        </Row>
      </Container>
    </PageSection>
    <PageSection background="primary">
      <Container>
        <h1>the word in classical literature, discovered the source.</h1>
        <p>
          the word in classical literature, discovered the undoubtable source.
          Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
          Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written
          in 45 BC. This book is a treatise on the theory of ethics, very
          popular during the Renaissance. The first line of Lorem Ipsum, "Lorem
          ipsum dolor sit amet..", comes from a line in section 1.10.32. The
          standard chunk of Lorem Ipsum used since the 1500s is reproduced below
          for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus
          Bonorum et Malorum" by Cicero are also reproduced in their exact
          original form, accompanied by English versions from the 1914
          translation by H. Rackham.
        </p>
      </Container>
    </PageSection>
    <PageSection space="small" background="grey-lighter">
      <Container>
        <h3>the word in classical literature, discovered the source.</h3>
        <p>
          the word in classical literature, discovered the undoubtable source.
          Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
        </p>
      </Container>
    </PageSection>
    <PageSection space="small">
      <Container>
        <p>
          the word in classical literature, discovered the undoubtable source.
          Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
        </p>
      </Container>
    </PageSection>
    <Container>
      <Row>
        <Col>
          <h1>the word in classical literature, discovered the source.</h1>
          <p>
            the word in classical literature, discovered the undoubtable source.
            Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
            Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
            written in 45 BC. This book is a treatise on the theory of ethics,
            very popular during the Renaissance. The first line of Lorem Ipsum,
            "Lorem ipsum dolor sit amet..", comes from a line in section
            1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is
            reproduced below for those interested. Sections 1.10.32 and 1.10.33
            from "de Finibus Bonorum et Malorum" by Cicero are also reproduced
            in their exact original form, accompanied by English versions from
            the 1914 translation by H. Rackham.
          </p>
          <h2>
            the word in classical literature, discovered the undoubtable source.
          </h2>
          <p>
            the word in classical literature, discovered the undoubtable source.
            Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
            Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
            written in 45 BC. This book is a treatise on the theory of ethics,
            very popular during the Renaissance. The first line of Lorem Ipsum,
            "Lorem ipsum dolor sit amet..", comes from a line in section
            1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is
            reproduced below for those interested. Sections 1.10.32 and 1.10.33
            from "de Finibus Bonorum et Malorum" by Cicero are also reproduced
            in their exact original form, accompanied by English versions from
            the 1914 translation by H. Rackham.
          </p>
          <h3>
            the word in classical literature, discovered the undoubtable source.
          </h3>
          <p>
            the word in classical literature, discovered the undoubtable source.
            Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
            Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
            written in 45 BC. This book is a treatise on the theory of ethics,
            very popular during the Renaissance. The first line of Lorem Ipsum,
            "Lorem ipsum dolor sit amet..", comes from a line in section
            1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is
            reproduced below for those interested. Sections 1.10.32 and 1.10.33
            from "de Finibus Bonorum et Malorum" by Cicero are also reproduced
            in their exact original form, accompanied by English versions from
            the 1914 translation by H. Rackham.
          </p>
          <ul>
            <li>1914 translation by H. Rackham.</li>
            <li>1914 translation by H. Rackham.</li>
            <li>1914 translation by H. Rackham.</li>
          </ul>
        </Col>
      </Row>
    </Container>
  </Layout>
);

export default KitchenPage;

export const query = graphql`
  fragment squareImage on File {
    childImageSharp {
      fluid(maxWidth: 480) {
        ...GatsbyImageSharpFluid
      }
    }
  }
  query {
    astronaut: file(relativePath: { eq: "gatsby-astronaut.png" }) {
      ...squareImage
    }
  }
`;
