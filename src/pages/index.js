import React from 'react';

import Layout from '../layouts/layout';
import { Seo } from '../components';

const IndexPage = () => (
  <Layout>
    <Seo title="Home" keywords={[`gatsby`, `application`, `react`]} />
    IndexPage
  </Layout>
);

export default IndexPage;
