import { createGlobalStyle } from 'styled-components';
import fontFiles from '../styles/fonts';
import { colors, fontSizes, breakpoints } from '../styles/theme';

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: "WorkSans";
    src: url(${fontFiles.WorkSansRegularEOT}) format("eot"),
    url(${fontFiles.WorkSansRegularTTF}) format("ttf"),
    url(${fontFiles.WorkSansRegularWOFF}) format("woff"),
    url(${fontFiles.WorkSansRegularWOFF2}) format("woff2");
  }

  @font-face {
    font-family: "WorkSans";
    font-weight: 300;
    src: url(${fontFiles.WorkSansLightEOT}) format("eot"),
    url(${fontFiles.WorkSansLightTTF}) format("ttf"),
    url(${fontFiles.WorkSansLightWOFF}) format("woff"),
    url(${fontFiles.WorkSansLightWOFF2}) format("woff2");
  }

  @font-face {
    font-family: "WorkSans";
    font-weight: 700;
    src: url(${fontFiles.WorkSansBoldEOT}) format("eot"),
    url(${fontFiles.WorkSansBoldTTF}) format("ttf"),
    url(${fontFiles.WorkSansBoldWOFF}) format("woff"),
    url(${fontFiles.WorkSansBoldWOFF2}) format("woff2");
  }

  * {
    box-sizing : border-box;
  }

  html, body {
    font-family: 'WorkSans';
    font-size: ${fontSizes.base};
    line-height: 1.5;
    color: ${colors.black};
  } 

  input:focus,button:focus, a:focus {
    outline: 2px solid ${colors.primary};
  }

  p {
    margin-bottom: 1.25em;
    margin-top: 0;
    font-size: 1rem;
  }

  h1, h2, h3, h4, h5 {
    margin: 0 0 1rem 0;
    line-height: 1.15;
  }

  h1 {
    font-size: 3.052rem;
  }

  h2 {
    font-size: 2.441rem;
  }

  h3 {
    font-size: 1.953rem;
  }

  h4 {
    font-size: 1.563rem;
  }

  h5 {
    font-size: 1.25rem;
  }

  @media screen and (max-width: ${breakpoints.m}) {
    html, body {
      font-size: 16px;
    }
  }

  ul {
    list-style-position: inside;
    padding: 0;
  }
`;
