import WorkSansBoldEOT from '../assets/fonts/WorkSans-Bold.eot';
import WorkSansBoldTTF from '../assets/fonts/WorkSans-Bold.ttf';
import WorkSansBoldWOFF from '../assets/fonts/WorkSans-Bold.woff';
import WorkSansBoldWOFF2 from '../assets/fonts/WorkSans-Bold.woff2';
import WorkSansLightEOT from '../assets/fonts/WorkSans-Light.eot';
import WorkSansLightTTF from '../assets/fonts/WorkSans-Light.ttf';
import WorkSansLightWOFF from '../assets/fonts/WorkSans-Light.woff';
import WorkSansLightWOFF2 from '../assets/fonts/WorkSans-Light.woff2';
import WorkSansRegularEOT from '../assets/fonts/WorkSans-Regular.eot';
import WorkSansRegularTTF from '../assets/fonts/WorkSans-Regular.ttf';
import WorkSansRegularWOFF from '../assets/fonts/WorkSans-Regular.woff';
import WorkSansRegularWOFF2 from '../assets/fonts/WorkSans-Regular.woff2';

export default {
  WorkSansBoldEOT,
  WorkSansBoldTTF,
  WorkSansBoldWOFF,
  WorkSansBoldWOFF2,
  WorkSansLightEOT,
  WorkSansLightTTF,
  WorkSansLightWOFF,
  WorkSansLightWOFF2,
  WorkSansRegularEOT,
  WorkSansRegularTTF,
  WorkSansRegularWOFF,
  WorkSansRegularWOFF2
};
