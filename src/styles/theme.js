export const colors = {
  primary: '#3993c7',
  grey: '#787878',
  greyLight: '#D0D0D0',
  greyLighter: '#f7f7f7',
  white: '#FFFFFF',
  black: '#23282d'
};

export const breakpoints = {
  xs: 0,
  s: '576px',
  m: '768px',
  l: '992px',
  xl: '1200px'
};

export const spaces = {
  xxxsmall: '4px',
  xxsmall: '8px',
  xsmall: '12px',
  small: '16px',
  default: '24px',
  large: '32px',
  xlarge: '48px',
  xxlarge: '64px',
  xxxlarge: '104px'
};

export const contentSpacing = '24px';

export const fontSizes = {
  base: '18px',
  small: '16px'
};

export const lineHeight = {
  base: 1.5
};

export const borderRadius = '5px';

export default {
  colors,
  breakpoints,
  spaces,
  fontSizes,
  lineHeight
};
