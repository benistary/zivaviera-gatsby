import React from 'react';
import styled from 'styled-components';
import { node, oneOf } from 'prop-types';

const StyledPageSection = styled.section`
  background-color: ${props =>
    (props.background === 'primary' && props.theme.colors.primary) ||
    (props.background === 'grey' && props.theme.colors.grey) ||
    (props.background === 'grey-lighter' && props.theme.colors.greyLighter)};
  padding: ${props =>
      (props.space === 'small' && props.theme.spaces.large) ||
      (props.space === 'large' && props.theme.spaces.xxlarge) ||
      props.theme.spaces.large}
    0;
  & > *:last-child {
    margin-bottom: 0;
  }
`;

const PageSection = ({ children, ...other }) => (
  <StyledPageSection {...other}>{children}</StyledPageSection>
);

PageSection.propTypes = {
  children: node,
  background: oneOf(['primary', 'grey', 'grey-lighter']),
  space: oneOf(['small', 'large'])
};

export default PageSection;
