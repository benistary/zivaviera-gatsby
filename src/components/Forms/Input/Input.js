import React, { useState } from 'react';
import styled from 'styled-components';
import { string, func } from 'prop-types';
import {
  lineHeight,
  fontSizes,
  spaces,
  contentSpacing,
  colors,
  borderRadius
} from '../../../styles/theme';
import Label from '../Label/Label';

const StyledInput = styled.input`
  line-height: ${lineHeight.base};
  font-size: ${fontSizes.small};
  padding: ${spaces.xsmall};
  margin-bottom: ${contentSpacing};
  background-color: ${colors.greyLighter};
  border: 0;
  border-radius: ${borderRadius};
  width: 100%;
  min-height: ${spaces.xlarge};
  ::placeholder {
    color: ${colors.grey};
  }
`;

const Input = ({
  value = '',
  id,
  placeholder,
  label,
  htmlType,
  onChange = () => {},
  ...other
}) => {
  const [inputValue, setInputValue] = useState(value);

  const handleChange = e => {
    setInputValue(e.target.value);
    onChange({ value: e.target.value });
  };

  const renderLabel = label && <Label id={id}>{label}</Label>;

  return (
    <>
      {renderLabel}
      <StyledInput
        id={id}
        type={htmlType}
        placeholder={placeholder}
        onChange={handleChange}
        {...other}
      />
    </>
  );
};

Input.propTypes = {
  value: string,
  id: string.isRequired,
  placeholder: string,
  label: string,
  onChange: func,
  htmlType: string
};

export default Input;
