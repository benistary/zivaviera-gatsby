import React from 'react';
import styled from 'styled-components';
import { node, string } from 'prop-types';
import { spaces, fontSizes } from '../../../styles/theme';

const StyledLabel = styled.label`
  display: block;
  font-size: ${fontSizes.small};
  margin-bottom: ${spaces.xxsmall};
`;

const Label = ({ children, id, ...other }) => (
  <StyledLabel htmlFor={id} {...other}>
    {children}
  </StyledLabel>
);

Label.propTypes = {
  children: node,
  id: string.isRequired
};

export default Label;
