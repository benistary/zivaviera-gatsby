import React from 'react';
import { node } from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { Normalize } from 'styled-normalize';
import { GlobalStyle } from '../styles/general';

import theme from '../styles/theme';

const Base = ({ children }) => (
  <>
    <Normalize />
    <GlobalStyle />
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
  </>
);

Base.propTypes = {
  children: node
};

export default Base;
