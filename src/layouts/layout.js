import React from 'react';
import { node } from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';
import Base from './_base';

const Layout = ({ children, data, ...other }) => (
  <Base>
    <main {...other}>{children}</main>
  </Base>
);

Layout.propTypes = {
  children: node
};

export default props => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => <Layout data={data} {...props} />}
  />
);
